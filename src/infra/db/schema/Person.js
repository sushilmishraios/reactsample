export const Person = {
  name: 'Person',
  primaryKey: 'Id',
  properties: {
    Id: {type: 'int', default: 1},
    FirstName: 'string',
    LastName: 'string',
    Email: 'string',
    JobTitle: 'string',
    Salary: 'string',
    IsFav: {type: 'bool', default: false}
  }
}