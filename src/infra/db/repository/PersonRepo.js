import DBHelper from '../DBHelper';
import {mapMultiplePersonDataToModelArray} from '../../../core/PersonModalMapper';


export default  class PersonRepo {

  constructor(){
    this.db = new  DBHelper();
  }


  addPerson(user){
    let realm = this.db.realm;
    let record = {
      Id: user.id,
      FirstName : user.fName,
      LastName : user.lName,
      Email : user.email,
      JobTitle : user.jobTitle,
      Salary : user.salary,
    };

    try {
      realm.write(() => {
        realm.create('Person', record, true);
      });
    }catch (e) {
      console.log(e);
    }
  }

  getAll() {
    // const query = Schema.PId + ' = ' + parentId
    let realm = this.db.realm;
    let records = realm.objects('Person');
    return mapMultiplePersonDataToModelArray(records);
  }

  getFav() {
    const query = 'IsFav' + ' = ' + true
    let realm = this.db.realm;
    let records = realm.objects('Person').filtered(query);
    return mapMultiplePersonDataToModelArray(records);
  }

  /**
   * AutoIncrement values
   * */
  getNextAutoIncrementValue(schemaName, key = 'Id') {
    let realm = this.db.realm;
    let records = realm.objects(schemaName).sorted(key, true);
    if (records && records.length > 0) {
      let first = records[0];
      return (first[key] + 1);
    }

    return 1;
  }



  /**
   * Update Values
   * */
  updateRecord(schemaName, id, key, value) {
    return new Promise((resolve, reject) => {
      try {
        let param = {
          Id: id
        };
        param[key] = value;
        let realm = this.db.realm;
        realm.write(() => {
          realm.create(schemaName, param, true);
        });
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }


}