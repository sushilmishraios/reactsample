import {Person} from './schema/Person';

const DBSchema = {
  Version: 5,
  Schema: [
    Person
  ]
};

export {DBSchema};