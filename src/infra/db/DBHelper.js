import Realm from 'realm';
import {DBSchema} from './DBSchema';


let instance = null;

export default class DBHelper {

  constructor() {
    this.realm = this.createSchema();
    instance = this;
    return instance;
  }


  createSchema() {

    const  schemas = DBSchema.Schema;
    const  dbVersion = DBSchema.Version;
    let options = {
      schema: schemas,
      schemaVersion: dbVersion,
    };
    let realm = new Realm(options);
    console.log(realm.path);
    return realm;
  }

  destroyInstance(){
    instance = null;
  }

  deleteAllRecords() {
    //Delete DB

    try{
      this.realm.write(() => {
        this.realm.deleteAll();
      });
    }catch(err){
      console.log(err);
    }
  }
}