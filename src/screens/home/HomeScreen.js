import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Modal
} from 'react-native';
import {AddPerson, fav, favSelected, filter, MenuIcon} from '../../core/ConstantIcon';
import appStyles from "../utils/AppStyle";
import PersonRepo from '../../infra/db/repository/PersonRepo';
import {EventRegister} from 'react-native-event-listeners';
import ModalView from '../utils/ModalView';
// var RNFS = require('react-native-fs');

export default class HomeScreen extends Component {
  /*
  * Constructor
  * */
  constructor(props) {
    super(props);
    this.personList = [];
    this.state={
      modalVisible: false,
      isUpdate: 0,
      filterSelected: 0
    }
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  /*
   * view life cycle
   * */
  static navigationOptions = ({navigation}) => {

    const actionAdd = navigation.getParam('actionAdd');
    const actionFilter = navigation.getParam('actionFilter');

    return {
      headerLeft: (
        <TouchableOpacity
          style={appStyles.menuButtonViewStyle}
          onPress={() => {
            navigation.openDrawer();
          }}>
          {MenuIcon(30)}
        </TouchableOpacity>
      ),
      headerRight: (
        <View style={{flexDirection: 'row'}}>
          <View>
            <TouchableOpacity
              style={appStyles.menuButtonViewStyle}
              onPress={() => {
                actionAdd()
              }}>
              {AddPerson(30)}
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              style={appStyles.menuButtonViewStyle}
              onPress={() => {
                actionFilter()
              }}>
              {filter(30)}
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  };

  componentDidMount(): void {
    this.props.navigation.setParams({'actionAdd': this.actionAdd.bind(this)});
    this.props.navigation.setParams({'actionFilter': this.actionFilter.bind(this)});

    this.reloadData()
  }

  actionAdd(){
    const data = {
      onCompletion: () => this.reloadData()
    };
    this.props.navigation.navigate("AddDetail", {data})
  }

  actionFilter(){
    this.setState({
      modalVisible: true
    })
  }


  reloadData(){
    let personRepo = new PersonRepo()
    this.personList = personRepo.getAll()
   this.setState({
     isUpdate: this.state.isUpdate + 1
   })
    EventRegister.emit('updateEvent', '')
  }

  filterData(value){
    this.setState({
      modalVisible: false,
      filterSelected: value
    })
    if (this.personList.length > 2){
      if (value === 1){
        this.personList.sort((a, b) => a.FirstName.localeCompare(b.FirstName));
      }
      else if(value === 2){
        this.personList.sort((a, b) => a.LastName.localeCompare(b.LastName));
      }
      else {
        this.personList.sort((a, b) => a.Id - b.Id);
      }

      this.setState({
        isUpdate: this.state.isUpdate + 1
      })
    }

  }

  render() {
    return (
      <View>
          <FlatList
            data={this.personList}
            renderItem={({item}) => this.renderDetail(item)}
          />
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
          }}>
          <ModalView
            value={this.state.filterSelected}
            selctedValue={(value)=> {
              console.log("radio button value ::::" + value)
             this.filterData(value)
            }}
          />
        </Modal>
      </View>
    )
  }

  renderDetail(item) {
    let firstNameChar = item.FirstName.slice(0,1).toUpperCase()
    let lastNameChar = item.LastName.slice(0,1).toUpperCase()

    let avtarStr = firstNameChar + lastNameChar

    return (
      <View style={{padding: 10}}>
        <View style={{padding: 10, backgroundColor: '#EDF1FF'}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{borderColor: 'green', borderWidth: 1, height: 40, width: 40, borderRadius: 20, justifyContent: 'center'}}>
                <Text style={{alignSelf: 'center', fontSize: 15, color: 'green'}}>{avtarStr}</Text>
              </View>
              <View style={{marginLeft: 10}}>
                <View style={{flexDirection: 'row'}}>
                  <Text>{item.FirstName}</Text>
                  <Text> </Text>
                  <Text>{item.LastName}</Text>
                </View>
                <Text>{item.JobTitle}</Text>
              </View>
            </View>
            <View style={{padding: 10, justifyContent: 'center'}}>
              <TouchableOpacity onPress={() => {
                this.updateFav(item)
              }}>
                {item.IsFav ? favSelected(40) : fav(40)}
              </TouchableOpacity>
            </View>
          </View>
        </View>

      </View>
    )
  }

  updateFav(item){
    let personRepo = new PersonRepo()
    personRepo.updateRecord('Person', item.Id, 'IsFav', !item.IsFav)
    this.reloadData()
  }

}
