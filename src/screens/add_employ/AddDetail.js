import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text
} from 'react-native';
import appStyles from "../utils/AppStyle";
import FloatingField from '../utils/Component/FloatingField';
import PersonRepo from '../../infra/db/repository/PersonRepo';

export default class AddDetail extends Component {
  /*
  * Constructor
  * */
  constructor(props) {
    super(props);
    const {navigation} = this.props;
    const data = navigation.getParam('data');
    if (data.onCompletion) {
      this.onCompletion = data.onCompletion;
    }

    this.state = {
      fName: '',
      lName: '',
      email: '',
      jobTitle: '',
      salary: ''
    }
  }
  render() {
    return(<View style={{flex: 1, padding: 20}}>
      <View style={{padding: 20, width: '100%', justifyContent: 'center'}}>
        <Text style={{alignSelf: 'center', fontSize: 22, fontWeight: 'bold', color: 'green'}}>Enter Employee Detail</Text>
      </View>
      <FloatingField
        label={'First Name'}
        onChangeText={(text) => {
          this.setState({
            fName: text
          })
        }}
      />
      <FloatingField
        label={'Last Name'}
        onChangeText={(text) => {
          this.setState({
            lName: text
          })
        }}
      />
      <FloatingField
        label={'Email'}
        onChangeText={(text) => {
          this.setState({
            email: text
          })
        }}
      />
      <FloatingField
        label={'Job Title'}
        onChangeText={(text) => {
          this.setState({
            jobTitle: text
          })
        }}
      />
      <FloatingField
        label={'Salary'}
        onChangeText={(text) => {
          this.setState({
            salary: text
          })
        }}
      />
      <View style={{ height: 40, width: '100%', backgroundColor: 'green', marginTop: 50}}>
        <TouchableOpacity
          style={{height: '100%', width: '100%', justifyContent: 'center'}}
          onPress={() => {this.actionSave()}}
        >
          <Text style={{alignSelf: 'center', color: 'white', fontSize: 15}}>Save Detail</Text>
        </TouchableOpacity>
      </View>
    </View>)
  }

  actionSave(){

    if (this.state.fName !== '' && this.state.lName !== '' && this.state.email !== '' && this.state.jobTitle !== '' && this.state.salary !== ''){
      let personRepo = new PersonRepo()
      let id = personRepo.getNextAutoIncrementValue('Person')
      let user = {id: id, fName: this.state.fName, lName: this.state.lName, email: this.state.email, jobTitle: this.state.jobTitle, salary: this.state.salary};
      personRepo.addPerson(user)
      if (this.onCompletion) {
        this.onCompletion();
      };
      this.props.navigation.goBack()
    }
    else {
      alert('Fill all detail');
    }

  }

}
