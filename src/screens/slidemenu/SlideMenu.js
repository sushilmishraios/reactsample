import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Text
} from 'react-native';
import {BackIcon} from '../../core/ConstantIcon';
import { EventRegister } from 'react-native-event-listeners'
import PersonRepo from '../../infra/db/repository/PersonRepo';


class Menu extends Component {

  constructor(props) {
    super(props);
    const {navigate} = this.props.itemProps;
    this.state = {
      totalEmp: 0,
      favEmp: 0,
    }
  }


  componentDidMount(): void {
    console.log("method calling:::::::::::")
    this.listener = EventRegister.addEventListener('updateEvent', (data) => {
      this.updateData();
    })
    this.updateData();
  }

  componentWillUnmount(): void {
    EventRegister.removeEventListener(this.listener)

  }

  updateData() {
    let personRepo = new PersonRepo();
    let personList = personRepo.getAll();
    let favList = personRepo.getFav();
    this.setState({
      totalEmp: personList.length,
      favEmp: favList.length
    })
  }

  /**
   * Render View
   * */
  render() {
    const itemProps = this.props.itemProps;
    return (
      <View style={{backgroundColor: 'green', flex: 1}}>
        <View style={{marginTop: 40, height: 50, width: 50, marginLeft: 50}}>
          <TouchableOpacity onPress={() => itemProps.navigation.closeDrawer()}>
            {BackIcon(40)}
          </TouchableOpacity>
        </View>
        <View style={{flex: 1, padding: 20}}>
          <Text style={{fontSize: 20, color: 'white', fontWeight: 'bold'}}>
            <Text>Total Person: </Text><Text>{this.state.totalEmp}</Text><Text>{'\n'}</Text><Text>Fav Person: </Text><Text>{this.state.favEmp}</Text>
          </Text>
        </View>
      </View>
    )
  }
}

export const SlideMenu = (props) => {
  return (
    <Menu itemProps={props}/>
  );
};