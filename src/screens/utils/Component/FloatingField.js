import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';

var FloatingLabel = require('react-native-floating-labels');

export default class FloatingField extends Component {

  /**
   * Constructor
   * */
  constructor(props) {
    super(props);

  }


  /**
   * Component Life Cycle
   * */
  componentWillMount(): void {

  }

  componentDidMount(): void {

  }


  /**
   * Render View
   * */
  render() {
    const {
      value, label
    } = this.props;
    return (
      <View>
        <FloatingLabel
          labelStyle={styles.labelInput}
          inputStyle={styles.input}
          style={styles.formInput}
          value={value}
          onBlur={this.onBlur}
          {...this.props}
          // onChangeText={(text) => {
          //     // const userName = text.trim();
          //     // this.setState({userName: userName})
          // }}
          underlineColorAndroid={'transparent'}
        >
          {label}
        </FloatingLabel>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  labelInput: {
    color: 'gray',
    fontSize: 13,
  },
  formInput: {
    borderBottomWidth: 1.5,
    borderColor: 'gray',
  },
  input: {
    borderWidth: 0,
    color: 'black',
    fontSize: 15,
  }
});