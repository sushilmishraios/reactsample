import {
  StyleSheet
} from 'react-native';

export default styles = StyleSheet.create({

  menuButtonViewStyle: {
    height: 45,
    width: 45,
    padding: 10,
  },
});