import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

var radio_props = [
  {label: 'Default', value: 0 },
  {label: 'First Name', value: 1 },
  {label: 'Last Name', value: 2 }

];
export default class ModalView extends Component {

  /**
   * Constructor
   * */
  constructor(props) {
    super(props);
  }

  render(){

    const {selctedValue, value} = this.props

    return(
      <View style={{backgroundColor: 'rgba(0, 0, 0, 0.4)', flex: 1, justifyContent: 'center'}}>
        <View style={{backgroundColor: 'white', height: 250, width: '80%', marginLeft: '10%', padding: 20, justifyContent: 'center'}}>
          <RadioForm
            radio_props={radio_props}
            initial={value}
            formHorizontal={false}
            labelHorizontal={true}
            buttonColor={'#2196f3'}
            animation={true}
            onPress={(value) => {selctedValue(value)}}
          />
        </View>
    </View>
    )
  }
}