
import React from "react";
import Icon from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign'
export const MenuIcon = (size = 20) => (<Icon name="md-menu" size={size} color='black' />);
export const AddPerson = (size = 20) => (<Icon name="md-person-add" size={size} color='black' />);
export const fav = (size = 20) => (<AntDesign name="staro" size={size} color='green' />);
export const favSelected = (size = 20) => (<AntDesign name="star" size={size} color='green' />);
export const BackIcon = (size = 20) => (<Icon name="md-arrow-back" size={size} color='white' />);
export const filter = (size = 20) => (<AntDesign name="filter" size={size} color='black' />);

