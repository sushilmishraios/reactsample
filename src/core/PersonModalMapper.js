export function mapPersonToModel(detail) {

  return {
    Id: detail.Id,
    FirstName: detail.FirstName,
    LastName: detail.LastName,
    Email: detail.Email,
    JobTitle: detail.JobTitle,
    Salary: detail.Salary,
    IsFav: detail.IsFav
  };
}

export function mapMultiplePersonDataToModelArray(personData) {
  return personData.map((el) => {
    return mapPersonToModel(el);
  });
}