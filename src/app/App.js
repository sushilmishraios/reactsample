/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {SlideMenu} from '../screens/slidemenu/SlideMenu';
import {createDrawerNavigator} from 'react-navigation-drawer';
import HomeScreen from '../screens/home/HomeScreen';
import AddDetail from '../screens/add_employ/AddDetail';

const Home = createStackNavigator({
    Home: {
      screen: HomeScreen,
      navigationOptions: {
      },
    },
    AddDetail: {
      screen: AddDetail,
      navigationOptions: {
      },
    }
  },
  {
    initialRouteName: 'Home',
  },
);

const App = createDrawerNavigator({
  Home
}, {
  contentComponent: SlideMenu
});

export default createAppContainer(App);
